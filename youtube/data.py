"""Data to keep between HTTP requests (app state)

* selected: list of selected videos
* selectable: list of selectable videos
"""

selected = []
selectable = []

PAGE = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <h1>Django YouTube (version 1)</h1>
    <h2>Selected</h2>
      <ul>
      {selected}
      </ul>
    <h2>Selectable</h2>
      <ul>
      {selectable}
      </ul>
  </body>
</html>
"""

VIDEO = """
      <li>
        <form action='/' method='post'>
          <a href='/{id}'>{title}</a>
          <input type='hidden' name='id' value='{id}'>
          <input type='hidden' name='csrfmiddlewaretoken' value='{token}'>
          <input type='hidden' name='{name}' value='True'> 
          <input type='submit' value='{action}'>
        </form>
      </li>
"""

VIDEO_ID = """
	<!DOCTYPE html>
	<html lang="en">
	  <body>
	    <h1><a href='/'>Django Youtube</a></h1>
	    <h3><a href='{link}'>{title}</a></h3>
	    <a href='{link}'><img src='{img}'></a>
	    <p>Youtube channel: <a href='{uri}'>{name}</a></p>
	    <p>Published date: {published}</p>
	    <p>Description: {description}</p>
	  </body>
	</html>
"""

ERROR = """
	<!DOCTYPE html>
	<html lang="en">
	  <body>
	    <meta http-equiv='refresh' content='1;url=/'>
	  </body>
	</html>
"""
